//Menu Toggle
const body = document.body;
const toggleAdd = document.getElementById('toggle__nav');
const toggleRemove = document.getElementById('toggle__nav__remove');
const toggle = document.getElementById('toggled__menu');

console.log(toggle, toggleRemove);

toggleAdd.addEventListener('click', () => {
    toggle.classList.add('show');
})

toggleRemove.addEventListener('click', () => {
    toggle.classList.remove('show');
})


//Carousel Boxes
const boxes = document.getElementById('boxes__container');
const prev = document.getElementById('prev');
const next = document.getElementById('next');
const box = document.querySelectorAll('#boxes__container .box');
const singleBox = document.getElementById('box');

let idx = 0;


function changeImage() {
    if (idx > box.length - 1) {
        idx = 0;
    }
    else if (idx < 0) {
        idx = box.length - 1;
    }
    else {
        let size = singleBox.offsetWidth + 8;

        boxes.style.transform = `translateX(${- idx * size}px)`;

        console.log(idx, size)
    }
}

next.addEventListener('click', () => {
    idx++;
    changeImage();
})

prev.addEventListener('click', () => {
    idx--;
    changeImage();
})


//FAQ 
const toggleButtons = document.querySelectorAll('.toggler');
const answersFaq = document.querySelectorAll('.faq__answer');

toggleButtons.forEach((toggleButton, i) => {
    toggleButton.addEventListener('click', () => {

        toggleButton.parentNode.classList.toggle('active');
        answersFaq[i].classList.toggle('show');

    })
})