const body = document.body;
const toggleAdd = document.getElementById('toggle__nav');
const toggleRemove = document.getElementById('toggle__nav__remove');
const toggle = document.getElementById('toggled__menu');

console.log(toggle, toggleRemove);

toggleAdd.addEventListener('click', () => {
    toggle.classList.add('show');
    body.classList.add('hidden');
})

toggleRemove.addEventListener('click', () => {
    toggle.classList.remove('show');
    body.classList.remove('hidden');
})