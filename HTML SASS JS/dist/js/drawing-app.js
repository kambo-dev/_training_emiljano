const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
const colorInput = document.getElementById('color');

let size = 20;
let isPressed = false;
let color = 'black';
let x;
let y;

canvas.addEventListener('mousedown', (e) => {
    isPressed = true;

    x = e.offsetX;
    y = e.offsetY;
  
})

canvas.addEventListener('mouseup', (e) => {
    isPressed = false;

    x = undefined;
    y = undefined;

})


canvas.addEventListener('mousemove', (e) => {
    if(isPressed) {
        const x2 = e.offsetX;
        const y2 = e.offsetY;

        drawCircle(x2, y2);
        drawLine(x, y, x2, y2);

        x = x2;
        y = y2;
    }

})

function drawCircle(x, y) {
    ctx.beginPath();
    ctx.arc(x, y, size, 0, Math.PI * 2);
    ctx.fillStyle = colorInput.value;
    ctx.fill();
}

function drawLine(x1, y1, x2, y2) {
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.strokeStyle = colorInput.value;
    ctx.lineWidth = size * 2;
    ctx.stroke();
}


const increase = document.getElementById('increase');
const decrease = document.getElementById('decrease');
const spanSize = document.getElementById('size');

increase.addEventListener('click', () => {
    if(size < 50) {
    size = size + 5;
    }
    else size = 50;

    spanSize.innerText = `${size}`;
})

decrease.addEventListener('click', () => {
    if(size > 5){
    size = size - 5;
    }
    else size = 5;

    spanSize.innerText = `${size}`;
})

const clearAll = document.getElementById('clear');
clearAll.addEventListener('click', () => {
    ctx.clearRect(0, 0, 800, 500);
})

